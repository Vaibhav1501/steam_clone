from django.http import HttpResponse
from django.shortcuts import render,redirect


def group_required(groups=[]):
    def decorator(view_func):
        def wrapper_func(request, *args, **kwargs):
            for group in request.user.groups.values():
                if group["name"] in groups:
                    # print(group['name'],groups)
                    return view_func(request,*args, ** kwargs)
            return render(request,'game/permission_denied.html')
        return wrapper_func
    return decorator


