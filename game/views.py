from django.shortcuts import render,redirect,get_object_or_404
from django.contrib.auth.decorators import login_required,permission_required
from game.decorators import group_required
from accounts.models import CustomUser
from .models import Game,Comment,UserGameInteraction
from django.db import transaction
from django.http import HttpResponse,JsonResponse,HttpResponseForbidden
from django.contrib import messages
import json
from django.views.decorators.csrf import csrf_exempt
from django.core.serializers import serialize
# Create your views here.

def home_page(request):
    games = Game.objects.all().values()
    return render(request,'game/homepage.html',context= {'games':games,})

@login_required(login_url='/login/')
# @permission_required('publish_game', raise_exception=True)
@group_required(['Power User'])
def upload_game(request):
    if request.method == "POST":
        title = request.POST.get('title')
        discription = request.POST.get('discription')
        thumbnail = request.FILES.get('thumbnail')
        trailer_video = request.FILES.get('trailer_video')
        price = request.POST.get('price')
        likes = 0
        dislikes = 0
        published_by = CustomUser.objects.filter(id = request.user.id)[0]
        print(published_by)
        try:
            with transaction.atomic():
                game = Game.objects.create(
                    title = title,
                    discription = discription,
                    thumbnail = thumbnail,
                    trailer_video = trailer_video,
                    price = price,
                    likes = 0,
                    dislikes = 0,
                    published_by = published_by,
                )
                print("Succefully created game object")
                messages.success(request, "Game uploaded successfully")
        except Exception as e:
            return HttpResponse("Some error occur "+str(e))
        return redirect('/')
    else:
        groups = request.user.groups.all().values()
        print(groups)
        return render(request,'game/gameform.html')
    
def permission_denied(request, exception=None):
    return render(request, 'game/permission_denied.html')

def game_info(request,game_id):
    game = Game.objects.filter(id=game_id).values()
    print(game)
    interaction =None
    if len(game)>0:
        if request.user.is_authenticated:
            interaction = UserGameInteraction.objects.filter(user=request.user.id, game=game_id).values()
            if len(interaction)>0:
                interaction = interaction[0]['action']
        print(interaction)
        user_id=game[0]['published_by_id']
        publisher = CustomUser.objects.get(id=user_id).username
        comments = Comment.objects.filter(game_id = game_id).values()
        return render(request,'game/gameinfopage.html',context={'game':game[0],'publisher':publisher,'interaction':interaction,'comments':comments})
    else:
        return HttpResponse("Invalid Game id or game does not exists")
    

@login_required(login_url='/login/')
def like_game(request, game_id):
    if request.method == 'POST':
        print("inside like game function")
        game = get_object_or_404(Game, id=game_id)
        interaction, created = UserGameInteraction.objects.get_or_create(user=request.user, game=game, defaults={'action': UserGameInteraction.LIKE})
        
        if not created and interaction.action == UserGameInteraction.DISLIKE:
            interaction.action = UserGameInteraction.LIKE
            interaction.save()
            game.likes += 1
            game.dislikes -= 1
        elif created:
            game.likes += 1
        
        game.save()
        return JsonResponse({'likes': game.likes, 'dislikes': game.dislikes})
    return HttpResponseForbidden()

@login_required(login_url='/login/')
def dislike_game(request, game_id):
    if request.method == 'POST':
        game = get_object_or_404(Game, id=game_id)
        interaction, created = UserGameInteraction.objects.get_or_create(user=request.user, game=game, defaults={'action': UserGameInteraction.DISLIKE})
        
        if not created and interaction.action == UserGameInteraction.LIKE:
            interaction.action = UserGameInteraction.DISLIKE
            interaction.save()
            game.dislikes += 1
            game.likes -= 1
        elif created:
            game.dislikes += 1
        
        game.save()
        return JsonResponse({'likes': game.likes, 'dislikes': game.dislikes})
    return HttpResponseForbidden()


def add_comment(request):
    print("reached")
    if request.method == 'POST':
        data =json.loads(request.body.decode('utf-8'))
        print(data)
        game_id = data['game_id']
        parent_id = data['parent_id']
        user_id = request.user.id
        content = data['content']
        print(user_id)

        comment = Comment.objects.create(user_id=user_id,parent_id=parent_id,game_id=game_id,content=content)
        print(comment)
        comment_data = {
            'id': comment.id,
            'user_id': comment.user_id,
            'parent_id': comment.parent_id_id,
            'game_id': comment.game_id,
            'content': comment.content,
            'created_at': comment.created_at.isoformat(),
            'likes': comment.likes,
            'dislikes': comment.dislikes
        }
        # comment = serialize('json',[comment])
        return JsonResponse(comment_data)
    
@login_required(login_url='/login/')
def delete_game(request,game_id):
    if request.method == 'DELETE':
        userinfo = CustomUser.objects.filter(id = request.user.id).values()
        if len(userinfo)>0:
            userinfo = userinfo[0]
            print(userinfo)
    
    return HttpResponse("success")

