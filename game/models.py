from django.db import models
# from django.contrib.auth.models import User
from accounts.models import *


# Create your models here.
class Game(models.Model):
    """ Defines database schema for Game"""
    title = models.CharField(max_length=200, unique=True)
    discription = models.TextField()
    release_date = models.DateTimeField(auto_now_add=True)
    thumbnail = models.ImageField(upload_to='thumbnails/')
    trailer_video = models.FileField(upload_to='videos/')
    price = models.FloatField()
    likes = models.IntegerField()
    dislikes = models.IntegerField()
    is_published = models.BooleanField(default=True)
    published_by = models.ForeignKey(CustomUser, on_delete=models.CASCADE)

    class Meta:
        permissions = [
            ('download_game', 'Can download game'),
            ('publish_game', 'Can publish game'),
        ]


class Comment(models.Model):
    """ Defines database schema for comments"""
    user = models.ForeignKey(CustomUser,on_delete=models.CASCADE)
    game = models.ForeignKey(Game,on_delete=models.CASCADE)
    parent_id = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, default=None, related_name='replies')
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)
    
    # class Meta:
    #     permissions = [
    #         ('add_comment', 'Can comment'),
    #     ]

class UserGameInteraction(models.Model):
    LIKE = 'like'
    DISLIKE = 'dislike'
    ACTION_CHOICES = [
        (LIKE, 'Like'),
        (DISLIKE, 'Dislike'),
    ]
    user = models.ForeignKey(CustomUser,on_delete=models.CASCADE)
    game = models.ForeignKey(Game,on_delete=models.CASCADE)
    action = models.CharField(max_length=7,choices=ACTION_CHOICES)

    class Meta:
        unique_together = ('user', 'game')