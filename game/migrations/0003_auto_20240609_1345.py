# Generated by Django 2.2.28 on 2024-06-09 13:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0002_auto_20240607_0905'),
    ]

    operations = [
        migrations.RenameField(
            model_name='game',
            old_name='publisher_by',
            new_name='published_by',
        ),
        migrations.AlterField(
            model_name='game',
            name='title',
            field=models.CharField(max_length=200, unique=True),
        ),
    ]
