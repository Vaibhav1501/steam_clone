from typing import Any
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User,Permission,Group
from django.contrib.contenttypes.models import ContentType
from game.models import *


class Command(BaseCommand):
    help = "Populates different groups and permissions"

    def handle(self, *args: Any, **options: Any) -> str | None:
        # create standard user group
        standard_user_group, _created = Group.objects.get_or_create(name='Standard User')
        
        # Create Power User group
        power_user_group, _created = Group.objects.get_or_create(name='Power User')
        
        # # Assign permissions to Standard User
        game_content_type = ContentType.objects.get_for_model(Game)
        comment_content_type = ContentType.objects.get_for_model(Comment)
        
        download_game_permission = Permission.objects.get(codename='download_game')
        comment_permission = Permission.objects.get(codename='add_comment')
        
        standard_user_group.permissions.add(download_game_permission, comment_permission)
        
        # Assign permissions to Power User
        publish_game_permission = Permission.objects.get(codename='publish_game')
        
        power_user_group.permissions.add(download_game_permission, comment_permission, publish_game_permission)
        print("Created groups")
        self.stdout.write(self.style.SUCCESS('Groups and permissions have been created.'))