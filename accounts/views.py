from django.shortcuts import render,redirect
from django.contrib import messages
from django.contrib.auth import login,logout ,authenticate
# from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from accounts.models import CustomUser
from game.models import Game
from django.db import transaction
from django.contrib.auth.models import Group
from django.http import HttpResponse,JsonResponse
# Create your views here.

def register_page(request):
    if request.method == "POST":
        # retriving all data
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        username = request.POST.get('username')
        email = request.POST.get('email')
        password = request.POST.get('password1')
        password2 = request.POST.get('password2')
        profile_picture = request.FILES.get('profile_picture')
        role = request.POST.get('role')
        print(username,password,email)

        # check if password matched or not
        if password != password2:
            messages.error(request,"Password did not matched!!!")
            return redirect('/register/')
        # check if username already taken or not
        user = CustomUser.objects.filter(username=username)
        if user.exists():
            messages.info(request, "Opps "+username+" already taken !!!")
            return redirect('/register/')
        with transaction.atomic():
            user= CustomUser.objects.create(
            first_name=first_name,
            last_name=last_name,
            username=username,
            email=email,
            profile_picture = profile_picture,
            )
            user.set_password(password)
            group = None
            if role == "standard":
                group = Group.objects.get(name = "Standard User")
            elif role == "power":
                group = Group.objects.get(name = "Power User")
            user.groups.add(group)
            user.save()
            messages.info(request,"Successfully registerd!!!!")
        return redirect('/login/')

    return render(request, 'accounts/registerpage.html')

def login_page(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = CustomUser.objects.filter(username=username)
        if user.exists():
            user =authenticate(username=username, password=password)
            if not user:
                messages.error(request=request,message="Invalid Password !!!")
            else:
                login(request=request,user=user)
                return redirect('/')
        else:
            messages.error(request,"Invalid username :"+username)
    return render(request, 'accounts/loginpage.html')

def logout_action(request):
    logout(request)
    return redirect('/')

def profile_page(request,user_id):
    if user_id is None:
        return redirect('/login/')
    user = CustomUser.objects.filter(id=user_id).values()
    if len(user)>0:
        user=user[0]
    else:
        user=None
    print(user)
    flag = False
    if request.user.id == user_id:
        flag = True
    group = request.user.groups.values()
    group = group[0]['name']
    games=None
    if flag and group == 'Power User':
        games = Game.objects.filter(published_by_id=user_id)
    print(games)
    return render(request,'accounts/profilepage.html',context={'user_info':user,'group':group,'games':games,'flag':flag})
